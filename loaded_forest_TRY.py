from IPython.display import Image
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

from joblib import dump, load
from sklearn.metrics import classification_report

from sklearn import datasets
from sklearn.model_selection import train_test_split

from sklearn.ensemble import RandomForestClassifier

random_state = 42

loaded_forest = load("./random_forest_model.joblib")

data_file = 'Wednesday-workingHours.pcap_ISCX.csv'
delimiter = ','

df = pd.read_csv(data_file, delimiter=delimiter, low_memory=False)
X = df.drop(['Flow Bytes/s', ' Flow Packets/s' ,' Label'], axis=1)
y = df[' Label']
Xtrain, Xtest, ytrain, ytest = train_test_split(X, y, random_state = random_state)
Xtest.tail()

ytest_forest = loaded_forest.predict(Xtest)    
print(classification_report(ytest, ytest_forest))

print(df.iloc[287560, ])

df.tail()


#dos hulk 287558 
dict = {' Destination Port': '80', ' Flow Duration': '1002', ' Total Fwd Packets':'2', ' Total Backward Packets': '0', 
        'Total Length of Fwd Packets': '0',' Total Length of Bwd Packets': '0', ' Fwd Packet Length Max': '0',
        ' Fwd Packet Length Min' :'0', ' Fwd Packet Length Mean':'0', ' Fwd Packet Length Std':'0',
        ' Bwd Packet Length Std':'0', ' Bwd Packet Length Mean':'0', 'Bwd Packet Length Max':'0', ' Bwd Packet Length Min':'0',
        ' Flow IAT Mean':'1002', ' Flow IAT Std':'0', ' Flow IAT Max':'1002',
        ' Flow IAT Min':'1002', 'Fwd IAT Total':'1002', ' Fwd IAT Mean':'1002',' Fwd IAT Std':'0', ' Fwd IAT Max':'1002', ' Fwd IAT Min':'1002',
        'Bwd IAT Total':'0',' Bwd IAT Mean':'0', ' Bwd IAT Std':'0', ' Bwd IAT Max':'0', ' Bwd IAT Min':'0', 
        'Fwd PSH Flags':'0', ' Bwd PSH Flags':'0', ' Fwd URG Flags':'0', ' Bwd URG Flags':'0', ' Fwd Header Length':'64',
        ' Bwd Header Length':'0', 'Fwd Packets/s':'1996007984', ' Bwd Packets/s':'0',' Min Packet Length':'0', ' Max Packet Length':'0',
        ' Packet Length Mean':'0', ' Packet Length Std':'0', ' Packet Length Variance':'0', 'FIN Flag Count':'0', ' SYN Flag Count':'0',
        ' PSH Flag Count':'0', ' RST Flag Count':'0', ' ACK Flag Count':'0', ' URG Flag Count':'0', ' CWE Flag Count':'0',
        ' ECE Flag Count':'0',
        ' Down/Up Ratio':'0', ' Average Packet Size':'0', ' Avg Bwd Segment Size':'0', ' Avg Fwd Segment Size':'0', 
        ' Fwd Header Length.1':'64', 'Fwd Avg Bytes/Bulk':'0', ' Fwd Avg Packets/Bulk':'0', ' Fwd Avg Bulk Rate':'0', 
        ' Bwd Avg Bytes/Bulk':'0', ' Bwd Avg Packets/Bulk':'0', 'Bwd Avg Bulk Rate':'0', 'Subflow Fwd Packets':'2', ' Subflow Fwd Bytes':'0',
        ' Subflow Bwd Packets':'0', ' Subflow Bwd Bytes':'0', 'Init_Win_bytes_forward':'274', ' Init_Win_bytes_backward':'-1',
        ' act_data_pkt_fwd':'0', ' min_seg_size_forward':'32', 'Active Mean':'0', ' Active Std':'0',' Active Max':'0', ' Active Min':'0',
        ' Idle Min':'0', ' Idle Std':'0',' Idle Max':'0','Idle Mean':'0'}

Xtest = Xtest.append(dict, ignore_index=True)

Xtest.tail()

ytest_WRITE = loaded_forest.predict(Xtest)

ytest_WRITE[-1]

dict2 = {' Destination Port': '135', ' Flow Duration': '69', ' Total Fwd Packets':'1', ' Total Backward Packets': '3', 
        'Total Length of Fwd Packets': '6',' Total Length of Bwd Packets': '18', ' Fwd Packet Length Max': '6',
        ' Fwd Packet Length Min' :'6', ' Fwd Packet Length Mean':'6', ' Fwd Packet Length Std':'0',
        ' Bwd Packet Length Std':'6', ' Bwd Packet Length Mean':'6', 'Bwd Packet Length Max':'6', ' Bwd Packet Length Min':'0',
        ' Flow IAT Mean':'23', ' Flow IAT Std':'36.3868108', ' Flow IAT Max':'65',
        ' Flow IAT Min':'1', 'Fwd IAT Total':'0', ' Fwd IAT Mean':'0',' Fwd IAT Std':'0', ' Fwd IAT Max':'0', ' Fwd IAT Min':'0',
        'Bwd IAT Total':'4',' Bwd IAT Mean':'2', ' Bwd IAT Std':'1.414213562', ' Bwd IAT Max':'3', ' Bwd IAT Min':'1', 
        'Fwd PSH Flags':'0', ' Bwd PSH Flags':'0', ' Fwd URG Flags':'0', ' Bwd URG Flags':'0', ' Fwd Header Length':'20',
        ' Bwd Header Length':'60', 'Fwd Packets/s':'14492.75362', ' Bwd Packets/s':'43478.26087',' Min Packet Length':'6', ' Max Packet Length':'6',
        ' Packet Length Mean':'6', ' Packet Length Std':'0', ' Packet Length Variance':'0', 'FIN Flag Count':'0', ' SYN Flag Count':'0',
        ' PSH Flag Count':'0', ' RST Flag Count':'0', ' ACK Flag Count':'1', ' URG Flag Count':'1', ' CWE Flag Count':'0',
        ' ECE Flag Count':'0',
        ' Down/Up Ratio':'3', ' Average Packet Size':'7.5', ' Avg Bwd Segment Size':'6', ' Avg Fwd Segment Size':'6', 
        ' Fwd Header Length.1':'20', 'Fwd Avg Bytes/Bulk':'0', ' Fwd Avg Packets/Bulk':'0', ' Fwd Avg Bulk Rate':'0', 
        ' Bwd Avg Bytes/Bulk':'0', ' Bwd Avg Packets/Bulk':'0', 'Bwd Avg Bulk Rate':'0', 'Subflow Fwd Packets':'1', ' Subflow Fwd Bytes':'6',
        ' Subflow Bwd Packets':'3', ' Subflow Bwd Bytes':'18', 'Init_Win_bytes_forward':'255', ' Init_Win_bytes_backward':'2052',
        ' act_data_pkt_fwd':'0', ' min_seg_size_forward':'20', 'Active Mean':'0', ' Active Std':'0',' Active Max':'0', ' Active Min':'0',
        ' Idle Min':'0', ' Idle Std':'0',' Idle Max':'0','Idle Mean':'0'}

Xtest = Xtest.append(dict2, ignore_index=True)

ytest_WRITE2 = loaded_forest.predict(Xtest)

ytest_WRITE2[-1]




