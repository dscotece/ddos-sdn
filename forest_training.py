from IPython.display import Image
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

from joblib import dump, load
from sklearn.metrics import classification_report

from sklearn import datasets
from sklearn.model_selection import train_test_split

from sklearn.ensemble import RandomForestClassifier


data_file = 'Wednesday-workingHours.pcap_ISCX.csv'
delimiter = ','

df = pd.read_csv(data_file, delimiter=delimiter, low_memory=False)

X = df.drop(['Flow Bytes/s', ' Flow Packets/s' ,' Label'], axis=1)
y = df[' Label']

Xtrain, Xtest, ytrain, ytest = train_test_split(X, y, random_state = 42)

print(Xtrain.shape, Xtest.shape, ytrain.shape, ytest.shape)

forest = RandomForestClassifier(criterion = 'entropy', n_estimators=20)
forest.fit(Xtrain, ytrain)

dump(forest, "./random_forest_model.joblib")





