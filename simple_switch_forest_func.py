# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
An OpenFlow 1.0 L2 learning switch implementation.
"""


from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_0
from ryu.lib.mac import haddr_to_bin
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib.packet import ipv4
from ryu.lib.packet import ipv6

import array 

#IMPORT PER ML
import pandas as pd

from joblib import dump, load
from sklearn.metrics import classification_report

from sklearn import datasets
from sklearn.model_selection import train_test_split

#inserire variabile per modello con global
global loaloaded_forest
loaded_forest = load("./random_forest_model.joblib")
data_file = 'Wednesday-workingHours.pcap_ISCX.csv'
delimiter = ','

df = pd.read_csv(data_file, delimiter=delimiter, low_memory=False)
X = df.drop(['Flow Bytes/s', ' Flow Packets/s' ,' Label'], axis=1)
y = df[' Label']
global Xtest
global ytest
Xtrain, Xtest, ytrain, ytest = train_test_split(X, y, random_state =42)

good_ip = ["10.0.0.1","10.0.0.2","10.0.0.3","10.0.0.4","10.0.0.5",
              "10.0.0.6","10.0.0.7","10.0.0.8","10.0.0.9","10.0.0.10"]
def create_pkt(src):
    if src in good_ip:
         dict = {' Destination Port': '80', ' Flow Duration': '69', ' Total Fwd Packets':'1', ' Total Backward Packets': '3',
                        'Total Length of Fwd Packets': '6',' Total Length of Bwd Packets': '18', ' Fwd Packet Length Max': '6',
                        ' Fwd Packet Length Min' :'6', ' Fwd Packet Length Mean':'6', ' Fwd Packet Length Std':'0',
                        ' Bwd Packet Length Std':'6', ' Bwd Packet Length Mean':'6', 'Bwd Packet Length Max':'6',
                        ' Bwd Packet Length Min':'0', ' Flow IAT Mean':'23', ' Flow IAT Std':'36.3868108',
                        ' Flow IAT Max':'65', ' Flow IAT Min':'1', 'Fwd IAT Total':'0', ' Fwd IAT Mean':'0',' Fwd IAT Std':'0',
                        ' Fwd IAT Max':'0', ' Fwd IAT Min':'0', 'Bwd IAT Total':'4',' Bwd IAT Mean':'2', ' Bwd IAT Std':'1.414213562',
                        ' Bwd IAT Max':'3', ' Bwd IAT Min':'1', 'Fwd PSH Flags':'0', ' Bwd PSH Flags':'0', ' Fwd URG Flags':'0',
                        ' Bwd URG Flags':'0', ' Fwd Header Length':'20', ' Bwd Header Length':'60', 'Fwd Packets/s':'14492.75362',
                        ' Bwd Packets/s':'43478.26087',' Min Packet Length':'6', ' Max Packet Length':'6', ' Packet Length Mean':'6',
                        ' Packet Length Std':'0', ' Packet Length Variance':'0', 'FIN Flag Count':'0', ' SYN Flag Count':'0',
                        ' PSH Flag Count':'0', ' RST Flag Count':'0', ' ACK Flag Count':'1', ' URG Flag Count':'1',
                        ' CWE Flag Count':'0', ' ECE Flag Count':'0', ' Down/Up Ratio':'3', ' Average Packet Size':'7.5',
                        ' Avg Bwd Segment Size':'6', ' Avg Fwd Segment Size':'6', ' Fwd Header Length.1':'20', 'Fwd Avg Bytes/Bulk':'0',
                        ' Fwd Avg Packets/Bulk':'0', ' Fwd Avg Bulk Rate':'0', ' Bwd Avg Bytes/Bulk':'0', ' Bwd Avg Packets/Bulk':'0',
                        'Bwd Avg Bulk Rate':'0', 'Subflow Fwd Packets':'1', ' Subflow Fwd Bytes':'6', ' Subflow Bwd Packets':'3',
                        ' Subflow Bwd Bytes':'18', 'Init_Win_bytes_forward':'255', ' Init_Win_bytes_backward':'2052', ' act_data_pkt_fwd':'0',
                        ' min_seg_size_forward':'20', 'Active Mean':'0', ' Active Std':'0',' Active Max':'0', ' Active Min':'0',
                        ' Idle Min':'0', ' Idle Std':'0',' Idle Max':'0','Idle Mean':'0'}       
    else: 
        dict = {' Destination Port': '80', ' Flow Duration': '1002', ' Total Fwd Packets':'2',
                    ' Total Backward Packets': '0', 'Total Length of Fwd Packets': '0',' Total Length of Bwd Packets': '0',
                    ' Fwd Packet Length Max': '0', ' Fwd Packet Length Min' :'0', ' Fwd Packet Length Mean':'0',
                    ' Fwd Packet Length Std':'0', ' Bwd Packet Length Std':'0', ' Bwd Packet Length Mean':'0',
                    'Bwd Packet Length Max':'0', ' Bwd Packet Length Min':'0', ' Flow IAT Mean':'1002', ' Flow IAT Std':'0',
                    ' Flow IAT Max':'1002', ' Flow IAT Min':'1002', 'Fwd IAT Total':'1002', ' Fwd IAT Mean':'1002',
                    ' Fwd IAT Std':'0', ' Fwd IAT Max':'1002', ' Fwd IAT Min':'1002', 'Bwd IAT Total':'0',' Bwd IAT Mean':'0',
                    ' Bwd IAT Std':'0', ' Bwd IAT Max':'0', ' Bwd IAT Min':'0', 'Fwd PSH Flags':'0', ' Bwd PSH Flags':'0',
                    ' Fwd URG Flags':'0', ' Bwd URG Flags':'0', ' Fwd Header Length':'64', ' Bwd Header Length':'0',
                    'Fwd Packets/s':'1996007984', ' Bwd Packets/s':'0',' Min Packet Length':'0', ' Max Packet Length':'0',
                    ' Packet Length Mean':'0', ' Packet Length Std':'0', ' Packet Length Variance':'0', 'FIN Flag Count':'0',
                    ' SYN Flag Count':'0', ' PSH Flag Count':'0', ' RST Flag Count':'0', ' ACK Flag Count':'0', ' URG Flag Count':'0',
                    ' CWE Flag Count':'0', ' ECE Flag Count':'0', ' Down/Up Ratio':'0', ' Average Packet Size':'0',
                    ' Avg Bwd Segment Size':'0', ' Avg Fwd Segment Size':'0', ' Fwd Header Length.1':'64', 'Fwd Avg Bytes/Bulk':'0',
                    ' Fwd Avg Packets/Bulk':'0', ' Fwd Avg Bulk Rate':'0', ' Bwd Avg Bytes/Bulk':'0', ' Bwd Avg Packets/Bulk':'0',
                    'Bwd Avg Bulk Rate':'0', 'Subflow Fwd Packets':'2', ' Subflow Fwd Bytes':'0',' Subflow Bwd Packets':'0',
                    ' Subflow Bwd Bytes':'0', 'Init_Win_bytes_forward':'274', ' Init_Win_bytes_backward':'-1',
                    ' act_data_pkt_fwd':'0', ' min_seg_size_forward':'32', 'Active Mean':'0', ' Active Std':'0',' Active Max':'0',
                    ' Active Min':'0', ' Idle Min':'0', ' Idle Std':'0',' Idle Max':'0','Idle Mean':'0'}
    return dict



class SimpleSwitch(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_0.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch, self).__init__(*args, **kwargs)
        self.mac_to_port = {}

    def add_flow(self, datapath, in_port, dst, src, actions):
        ofproto = datapath.ofproto

        match = datapath.ofproto_parser.OFPMatch(
            in_port=in_port,
            dl_dst=haddr_to_bin(dst), dl_src=haddr_to_bin(src))

        mod = datapath.ofproto_parser.OFPFlowMod(
            datapath=datapath, match=match, cookie=0,
            command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0,
            priority=ofproto.OFP_DEFAULT_PRIORITY,
            flags=ofproto.OFPFF_SEND_FLOW_REM, actions=actions)
        datapath.send_msg(mod)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocol(ethernet.ethernet)
        
     #   pkt_array = packet.Packet(array.array('B', ev.msg.data))
     #   for p in pkt_array:
     #       print (p)


        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        dst = eth.dst
        src = eth.src


        pkt_ipv4 = pkt.get_protocol(ipv4.ipv4)
        if pkt_ipv4 is not  None:
           # pkt_ipv6 =pkt.get_protocol(ipv6.ipv6)
            self.logger.info("packet in from %s",pkt_ipv4.src)
            dict = create_pkt(pkt_ipv4.src)
            global Xtest
            Xtest = Xtest.append(dict, ignore_index=True)
            ytest_WRITE = loaded_forest.predict(Xtest)
            self.logger.info("%s!",ytest_WRITE[-1])


        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})

       # self.logger.info("packet in %s",pkt_ipv4.src)
       
       #self.logger.info("packet in %s", dpid, src_ipv4, dst_ipv4, msg.in_port)
 
        msg_tojson = msg.to_jsondict()
        #self.logger.info("json:%s",msg_tojson)



        # learn a mac address to avoid FLOOD next time.
        self.mac_to_port[dpid][src] = msg.in_port

        if dst in self.mac_to_port[dpid]:
            out_port = self.mac_to_port[dpid][dst]
        else:
            out_port = ofproto.OFPP_FLOOD

        actions = [datapath.ofproto_parser.OFPActionOutput(out_port)]

        # install a flow to avoid packet_in next time
        if out_port != ofproto.OFPP_FLOOD:
            self.add_flow(datapath, msg.in_port, dst, src, actions)

        data = None
        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        out = datapath.ofproto_parser.OFPPacketOut(
            datapath=datapath, buffer_id=msg.buffer_id, in_port=msg.in_port,
            actions=actions, data=data)
        datapath.send_msg(out)

    @set_ev_cls(ofp_event.EventOFPPortStatus, MAIN_DISPATCHER)
    def _port_status_handler(self, ev):
        msg = ev.msg
        reason = msg.reason
        port_no = msg.desc.port_no

        ofproto = msg.datapath.ofproto
        if reason == ofproto.OFPPR_ADD:
            self.logger.info("port added %s", port_no)
        elif reason == ofproto.OFPPR_DELETE:
            self.logger.info("port deleted %s", port_no)
        elif reason == ofproto.OFPPR_MODIFY:
            self.logger.info("port modified %s", port_no)
        else:
            self.logger.info("Illeagal port state %s %s", port_no, reason)
